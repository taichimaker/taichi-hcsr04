Arduino Taichi-HCSR04 库
========
by 太极创客 Taichi-Maker / www.taichi-maker.com
2019-06-15

中文说明
--------
本Arduino库旨在读取HCSR04超声传感器模块所测量到的距离数值。
测量结果有厘米和英寸两种模式。
English Description
--------------------
This Arduino Lib is designed for the HCSR04 ultra sound distance sensor.
Measurement can be CM or Inch.
